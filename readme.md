## Status
![npm (scoped)](https://img.shields.io/npm/v/@shadoll/dandelion)
![npm](https://img.shields.io/npm/dm/@shadoll/dandelion)
![NPM](https://img.shields.io/npm/l/@shadoll/dandelion)

## install

## using

```js

import Vue from 'vue';
import {LimeMenu} from '@shadoll/dandelion';

Vue.use(LimeMenu);


```

 **template**
 
 ```vue

<template>
    <LeftMenu
     :items="items"
     :collapse="collapse"
     :opened="opened"
     :roles="roles">
    </LeftMenu>
</template>

<script>

 export default {

    data: () => ({
        items: [], // models of SingleLimeMenuModel
        collapse: false,
        opened: ['uuid-1', 'uuid-4'],
        roles: ['admin', 'root'],
    })
}

</script>
```
