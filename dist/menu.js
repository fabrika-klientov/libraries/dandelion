"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LimeMenu = void 0;
var LimeMenu = /** @class */ (function () {
    function LimeMenu() {
    }
    LimeMenu.install = function (vue) {
        vue.component('LeftMenu', require('../components/left-menu.vue').default);
    };
    return LimeMenu;
}());
exports.LimeMenu = LimeMenu;
