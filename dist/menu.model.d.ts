export interface SingleLimeMenuModel {
    uuid?: string;
    parent_uuid?: string | null;
    icon?: string;
    type: 'element' | 'group';
    title: string;
    link?: string;
    visible: boolean;
    disabled: boolean;
    sort: number;
    roles: any[];
}
export interface LimeMenuModel extends SingleLimeMenuModel {
    active: boolean;
    children?: LimeMenuModel[];
    parent: LimeMenuModel | null;
}
export interface UserModel {
    name: string;
    email: string;
    user_name: string;
}
