export class LimeMenu {

    static install(vue: any): void {
        vue.component('LeftMenu', require('../components/left-menu.vue').default)
    }
}
